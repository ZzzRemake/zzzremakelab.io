---
title: docker-first
top: false
cover: false
mathjax: true
date: 2022-11-04 00:14:08
summary: 供docker 某些指令的查询
tags: 
    - docker
    - primary
categories: docker
---
本文记录指令信息供之后查询。

## begin

link:[docker-docs](https://docs.docker.com/)

### docker build

```shell
docker build -t tag-name .
```

`build`: 用`dockerfile`来建立映像。
`-t` flag your image,后面跟着名字。
`.` current directory to find `Dockerfile`

### docker run

```shell
docker run -dp 3000:3000 tag-name
```

`run` 运行docker
`-d` 分离“detached”mode运行。
`-p` map from host's port to container's port

### update code

```shell
docker ps
# swap out container-id with ID from docker ps
docker stop container-id
docker rm container-id
# or stop&rm:docker rm -f container-id
```

`ps`：运行中容器list
`stop` stop 运行
`rm`：remove container（默认需要stop后）
`-f`：强制。

以上仍然需要麻烦的持续性重头再来。

### push docker-hub

`docker-hub`:先创个仓库。后面会直接给你个代码：

```shell
docker push namespace/tagname
```

然而只有这个是不够的。

```shell
docker login -u USER-NAME
docker tag image-name USER-NAME/NEW-NAME
docker push USER-NAME/NEW-NAME
```

tag一下，才能找到正确的image捏。

### DB

一个容器运行的时候，他是用映像里头的多个层作为文件系统的，每个容器还有自己的暂存空间来创建/更新/删除文件，任何更改都不会在另一个容器看到。
比如：

```shell
docker run -d ubuntu bash -c "shuf -i 1-10000 -n 1 -o /data.txt && tail -f /dev/null"
docker exec container-id cat /data.txt
```

then, we can use volume!

### named volumes

数据bucket，持久化指定位置的最后改动数据，

用卷持久化：

```shell
docker volume create volume-name
docker run -dp 3000:3000 -v volume-name:<where?> <container-name>
```

where is the volume?

```shell
docker volume inspect volume-name
```

### bind mounts

named volume持续化数据还是不错的。
这里讲的bind mounts，可以控制确切的挂载点。我们可以用其持久化数据，但它通常用于提供额外的数据给容器。用其可以立刻看到所需的更改。

!!! note various volume
    当然，其中还是有别的volume的，这里只有bind mounts和named volumed。

```shell
docker run -dp 3000:3000 \
     -w /app -v "$(pwd):/app" \
     node:12-alpine \
     sh -c "yarn install && yarn run dev"
```

`-w app`：set working directory
`-v "$(pwd):/app"`bind mount the current（pwd？）directory into the `/app` directory in the container
`node:12-alpline` image to use（base image）
`sh -c....` alpine：shell running command。

查看docker日志：

```shell
docker logs -f container-id
```

### 多应用：多容器

#### container networking

两种方式：

1. 开始的时候给你来个网
2. 连接现成的container

```shell
docker network create network-name
# create network，and then：
docker run -d \
     --network todo-app --network-alias mysql \
     -v todo-mysql-data:/var/lib/mysql \
     -e MYSQL_ROOT_PASSWORD=secret \
     -e MYSQL_DATABASE=todos \
     mysql:5.7
```

可以看到，`-e`设置了环境变量.

!!! note new volume?
    在这里,自动创建了个volume，因此出现了从未出现的卷名。

```shell
docker run -it --network todo-app nicolaka/netshoot
```

use dig tool,we could find the mysql `ip`，docker可以将名字和网络地址联系：use`--network-alias`flag。

#### connect mysql with app

```shell
# mysql version8.0 and higher:
mysql> ALTER USER 'root' IDENTIFIED WITH mysql_native_password BY 'secret';
mysql> flush privileges;
mysql> exit
# docker:connect
docker run -dp 3000:3000 \
   -w /app -v "$(pwd):/app" \
   --network todo-app \
   -e MYSQL_HOST=mysql \
   -e MYSQL_USER=root \
   -e MYSQL_PASSWORD=secret \
   -e MYSQL_DB=todos \
   node:12-alpine \
   sh -c "yarn install && yarn run dev"
# operate, and show mysql imformation
docker exec -it <mysql-container-id> mysql -p todos
mysql> select * from todo_items;
 +--------------------------------------+--------------------+-----------+
 | id                                   | name               | completed |
 +--------------------------------------+--------------------+-----------+
 | c906ff08-60e6-44e6-8f49-ed56a0853e85 | Do amazing things! |         0 |
 | 2912a79e-8486-4bc3-a4c5-460793a575ab | Be awesome!        |         0 |
 +--------------------------------------+--------------------+-----------+
```

### Docker Compose

[install Docker Compose](https://docs.docker.com/compose/install/)

and then: create `docker-compose.yml` in root directory.

!!! info 不想写了，看连接吧
    you can see detail in [here](https://docs.docker.com/get-started/08_using_compose/)

### addition

#### 扫描安全漏洞

```shell
docker scan --login
docker scanf image-name
```

#### 查看Image Layering

```shell
docker image histroy image-name
#  可以看到Image-id,create-time,create-by,size,comment
docker image history --no-trunc getting-started
# 忽略截断捏。
```

!!! warning layer changes
    Once a layer changes, all downstream layers have to be recreated as well

#### .dockerignore

.dockerignore文件中：

```docker
node_modules
```

使用缓存speed up。

#### multi-stage builds

```dockerfile
# syntax=docker/dockerfile:1
FROM maven AS build
WORKDIR /app
COPY . .
RUN mvn package

FROM tomcat
COPY --from=build /app/target/file.war /usr/local/tomcat/webapps

# react example
# syntax=docker/dockerfile:1
FROM node:12 AS build
WORKDIR /app
COPY package* yarn.lock ./
RUN yarn install
COPY public ./public
COPY src ./src
RUN yarn run build

FROM nginx:alpine
COPY --from=build /app/build /usr/share/nginx/html
```

多阶段构建可以减少整体image size。
